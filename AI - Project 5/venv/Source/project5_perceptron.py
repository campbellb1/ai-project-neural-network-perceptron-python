"""
project5_perceptron.py
Programmer: Brandon Campbell (With Framework By Dr. Bennett)
Date:       4/27/2018
Purpose:    The purpose of this file is to function as a perceptron for the neural network.
            This perceptron 'trains' on a dataset and attempts to predict a certain output
            based
"""

import numpy as np
import pandas as pd
import math as mth
import random as rand
class Perceptron:
    """ Perceptron classifier """

    def __init__(self, n_inputs, learning_rate=0.4, iterations=20):
        self.n_inputs = n_inputs
        self.learning_rate = learning_rate
        self.iterations = iterations
        self.w_ = np.zeros(1)
        self.errors_ = []


    def train(self, X, y):
        """
            Fits the training data to the Perceptron
        """

        #TODO: Code the Perceptron Learning Algorithm
        # (1)	Initialize weights to zero or a small random number.
        #        Don't forget w_[0] is the bias weight
        # (2)	For the number of iterations,
        #       (a)	For each item in data set X:
        #         (i)	Activate the answer
        #         (ii)	Calculate the error based on the answer and the known
        #                  y[i]
        #         (iii)	Update the bias weight and input weights if the
        #                  activated answer is erroneous
        #       (b)	Record the error in self.errors_

        self.w_ = np.array([0.0 for x in range(len(X[0]) + 1)])
        print(self.w_)
        for i in range(self.iterations):
            errors = 0
            # for xVal, yVal in zip(X, y):
            #     result = self.activate(xVal)
            #     error = self.learning_rate * (yVal - result)
            #     if error != 0:
            #         errors = errors + 1
            #     # 𝑊(𝑖)=𝑊(𝑖)+𝛼[𝑇−𝐴]∗𝑃(𝑖)
            #     self.w_[1:] += np.multiply(error, xVal)
            #     self.w_[0] += error
            # self.errors_.append(errors)

            for counter in range(len(X)):
                result = self.activate(X[counter])
                error = self.learning_rate * (y[counter] - result)
                if error != 0:
                    errors = errors + 1
                # 𝑊(𝑖)=𝑊(𝑖)+𝛼[𝑇−𝐴]∗𝑃(𝑖)
                self.w_[1:] += np.multiply(error, X[counter])
                self.w_[0] += error
            self.errors_.append(errors)
    def net_input(self, X):
        """ Calculate the Net Input """
        # TODO: Calculate the sum of the product of each input and each weight
        dotProduct = np.dot(X, self.w_[1:])
        # dotProduct = sum(i*j for i, j in zip(X, self.w_[1:]))
        dotProduct += self.w_[0]
        return dotProduct

    def activate(self, X):
        """ STEP FUNCTION: Returns the class label after the unit step """
        # TODO: Return 1 if net_input(X) >= 0.0 or -1 otherwise
        if self.net_input(X) >= 0.0:
            return 1
        else:
            return -1
